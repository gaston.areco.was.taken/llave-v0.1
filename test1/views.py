from django.shortcuts import render


def home(request):
    return render(request, 'home.html')


def article(request):
    return render(request, 'article.html')


def contact(request):
    return render(request, 'contact.html')


def login(request):
    return render(request, 'login.html')


def branch_offices(request):
    return render(request, 'branch_offices.html')


def faqs(request):
    return render(request, 'faqs.html')


def privacy(request):
    return render(request, 'privacy.html')

def registrationForm(request):
    return render(request, 'registrationForm.html')

def error(request):
    return render(request, 'error.html')

def management(request):
    return render(request, 'management.html')

def list(request):
    return render(request, 'list.html')
