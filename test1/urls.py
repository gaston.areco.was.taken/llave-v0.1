"""test1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('', views.home, name='home'),
    path('article/', views.article, name='article'),
    path('contact/', views.contact, name='contact'),
    path('login/', views.login, name='login'),
    path('branch_offices/', views.branch_offices, name='branch_offices'),
    path('faqs/', views.faqs, name='faqs'),
    path('privacy/', views.privacy, name='privacy'),
    path('registrationForm/', views.registrationForm, name='registrationForm'),
    path('error/', views.error, name='error'),
    path('list/', views.list, name='list'),
    path('management/', views.management, name='management'),
    path('admin/', admin.site.urls),
]
